module "ssh-keypair" {
  source              = "./ssh-keypair"
  product             = var.product
  use_case            = var.use_case
  environment         = var.environment
  storage_bucket_name = var.ssh_key_bucket_name
  save_private_key    = var.save_private_key
}

module "cluster_encryption_kms_key" {
  source      = "cloudposse/kms-key/aws"
  version     = "0.12.1"
  policy      = var.cluster_config.cluster_encryption_kms_key_policy
  alias       = "alias/${local.name}-cluster-secrets-kms-key"
  description = "${local.name}-cluster-secrets-kms-key"
}

data "aws_eks_cluster_auth" "eks_auth" {
  name = module.eks.cluster_id
}

provider "kubernetes" {
  host                   = module.eks.cluster_endpoint
  cluster_ca_certificate = base64decode(module.eks.cluster_certificate_authority_data)
  token                  = data.aws_eks_cluster_auth.eks_auth.token
}

module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "18.26.4"

  aws_auth_accounts = var.cluster_config.aws_auth_accounts
  aws_auth_roles    = var.cluster_config.aws_auth_roles
  aws_auth_users    = var.cluster_config.aws_auth_users

  cloudwatch_log_group_kms_key_id        = var.cluster_config.cloudwatch_log_group_kms_key_id
  cloudwatch_log_group_retention_in_days = var.cluster_config.cloudwatch_log_group_retention_in_days

  cluster_additional_security_group_ids = var.cluster_config.cluster_additional_security_group_ids
  cluster_addons                        = var.cluster_config.cluster_addons
  cluster_enabled_log_types             = var.cluster_config.cluster_enabled_log_types

  cluster_encryption_config = [{
    provider_key_arn = module.cluster_encryption_kms_key.key_arn
    resources        = ["secrets"]
  }]

  attach_cluster_encryption_policy      = var.cluster_config.attach_cluster_encryption_policy
  cluster_encryption_policy_description = "${local.name}-encryption-policy"
  cluster_encryption_policy_name        = "${local.name}-enc-policy"

  cluster_endpoint_private_access      = var.cluster_config.cluster_endpoint_private_access
  cluster_endpoint_public_access       = var.cluster_config.cluster_endpoint_public_access
  cluster_endpoint_public_access_cidrs = var.cluster_config.cluster_endpoint_public_access_cidrs

  cluster_ip_family = "ipv4"
  cluster_name      = "${local.name}-cluster"

  cluster_security_group_additional_rules = var.cluster_config.cluster_security_group_additional_rules
  cluster_security_group_description      = "${local.name}-cluster-sg"
  cluster_security_group_name             = "${local.name}-cluster-sg"

  cluster_service_ipv4_cidr = var.cluster_config.cluster_service_ipv4_cidr

  cluster_timeouts = var.cluster_config.cluster_timeouts
  cluster_version  = var.cluster_config.cluster_version

  control_plane_subnet_ids = var.cluster_config.control_plane_subnet_ids

  eks_managed_node_group_defaults = local.eks_managed_node_group_defaults
  eks_managed_node_groups         = local.eks_managed_node_groups

  iam_role_additional_policies = var.cluster_config.iam_role_additional_policies
  iam_role_description         = "${local.name}-cluster-iam-role"
  iam_role_name                = "${local.name}-cluster-iam-role"

  create_kms_key                  = var.cluster_config.create_kms_key
  kms_key_administrators          = var.cluster_config.kms_key_administrators
  kms_key_deletion_window_in_days = var.cluster_config.kms_key_deletion_window_in_days
  kms_key_description             = "${local.name}-cluster-key"
  kms_key_enable_default_policy   = var.cluster_config.kms_key_enable_default_policy
  enable_kms_key_rotation         = var.cluster_config.enable_kms_key_rotation
  kms_key_owners                  = var.cluster_config.kms_key_owners
  kms_key_service_users           = var.cluster_config.kms_key_service_users
  kms_key_users                   = var.cluster_config.kms_key_users
  kms_key_source_policy_documents = var.cluster_config.kms_key_source_policy_documents

  node_security_group_additional_rules = var.cluster_config.node_security_group_additional_rules
  node_security_group_description      = "${local.name}-node-sg"
  node_security_group_name             = "${local.name}-node-sg"
  node_security_group_tags             = local.node_security_group_tags

  node_security_group_ntp_ipv4_cidr_block = ["169.254.169.123/32"]
  node_security_group_ntp_ipv6_cidr_block = ["fd00:ec2::123/128"]

  openid_connect_audiences = var.cluster_config.openid_connect_audiences
  custom_oidc_thumbprints  = var.cluster_config.custom_oidc_thumbprints

  cluster_identity_providers = var.cluster_config.cluster_identity_providers

  manage_aws_auth_configmap = var.cluster_config.manage_aws_auth_configmap
  create_aws_auth_configmap = var.cluster_config.create_aws_auth_configmap

  subnet_ids = var.cluster_config.node_subnet_ids
  vpc_id     = var.cluster_config.vpc_id
}