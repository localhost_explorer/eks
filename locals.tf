locals {
  tags = {
    Environment             = var.environment
    Product                 = var.product
    Use_case                = var.use_case
    Can_be_deleted          = true
    Created_using_terraform = true
  }
  name = "${var.environment}-${var.product}-${var.use_case}"
  extra_node_group_configs = {
    vpc_id               = var.cluster_config.vpc_id
    subnet_ids           = var.cluster_config.node_subnet_ids
    key_name             = module.ssh-keypair.key_name
    launch_template_tags = local.tags
  }
  eks_managed_node_group_defaults = { for k, v in var.cluster_config.eks_managed_node_group_defaults : k => merge(v, local.extra_node_group_configs, {
    security_group_name         = "${local.name}-${k}-ng-sg"
    security_group_description  = "${local.name}-${k}-ng-sg"
    launch_template_name        = "${local.name}-${k}-lt"
    launch_template_description = "${local.name}-${k}-lt"
    iam_role_name               = "${local.name}-${k}-iam-role"
    iam_role_description        = "${local.name}-${k}-iam-role"
    tags = var.need_karpenter_tags ? {
      "karpenter.sh/discovery/${local.name}-cluster" = "${local.name}-cluster"
    } : {}
  }) }
  eks_managed_node_groups = { for k, v in var.cluster_config.eks_managed_node_groups : k => merge(v, local.extra_node_group_configs, {
    security_group_name         = "${local.name}-${k}-ng-sg"
    security_group_description  = "${local.name}-${k}-ng-sg"
    launch_template_name        = "${local.name}-${k}-lt"
    launch_template_description = "${local.name}-${k}-lt"
    iam_role_name               = "${local.name}-${k}-iam-role"
    iam_role_description        = "${local.name}-${k}-iam-role"
    tags = var.need_karpenter_tags ? {
      "karpenter.sh/discovery/${local.name}-cluster" = "${local.name}-cluster"
    } : {}
  }) }
  minimal_node_security_group_tags = merge(
    var.node_security_group_tags, {
      "kubernetes.io/cluster/${local.name}-cluster" = null
    }
  )
  node_security_group_tags = var.need_karpenter_tags ? merge(
    local.minimal_node_security_group_tags, {
      "karpenter.sh/discovery/${local.name}-cluster" = "${local.name}-cluster"
    }
  ) : local.minimal_node_security_group_tags
}